#!/bin/bash
# mock script to simulate output of ciscodnacbackupctl list

if [ $# -ne 1 ]; then
    echo "Mock (list) syntax error: one parameter is requred"
    exit 1 
fi

COMMAND=$1

if [ $COMMAND != "list" ]; then
    echo "Mock (list) syntax error: first paramter must be <list>, was: <$COMMAND>"
    exit 1 
fi

echo "BACKUP_ID                             BACKUP_SIZE    COMPATIBLE    DESCRIPTION    END_TIMESTAMP        START_TIMESTAMP      STATUS"
echo "9902ebc8-e4d1-48d0-b8e4-ceab49641a82  107G           TRUE          daily-backup   2021-05-11 23:04:15  2021-05-11 23:01:02  SUCCESS"
echo "c3d09320-2203-48c3-a8f8-836b8ff31b3f  109G           TRUE          daily-backup   2021-05-12 23:05:08  2021-05-12 23:01:04  SUCCESS"
echo "921a7a87-caf3-4a9b-9c91-4b0d8a06a6e9  111G           TRUE          daily-backup   2021-05-13 23:05:04  2021-05-13 23:01:01  SUCCESS"
echo "148e0e6a-0a28-4c8f-a5d1-d86fafcbce7b  112G           TRUE          daily-backup   2021-05-14 23:04:10  2021-05-14 23:01:03  SUCCESS"
echo "b1707df1-a4b0-4723-bd1b-02c0ee2ca243  86G            TRUE          daily-backup   2021-05-15 23:04:08  2021-05-15 23:00:59  SUCCESS"
echo "5d8cf7ba-9ac7-4a5e-a17b-20fa7601d613  80G            TRUE          daily-backup   2021-05-16 23:04:17  2021-05-16 23:01:07  SUCCESS"
echo "22ef922f-11e3-4271-9c19-b56fe4f172e4  82G            TRUE          daily-backup   2021-05-17 23:04:13  2021-05-17 23:01:02  SUCCESS"
echo "74879fe5-7627-47af-a879-fb138430b53a  85G            TRUE          daily-backup   2021-05-18 23:04:14  2021-05-18 23:01:04  SUCCESS"

