#!/bin/bash
# mock script to simulate output of ciscodnacbackupctl delete ID

if [ $# -ne 2 ]; then
    echo "Mock (delete) syntax error: two parameters are requred"
    exit 1 
fi

COMMAND=$1
ID=$2

if [ $COMMAND != "delete" ]; then
    echo "Mock (delete) syntax error: first paramter must be <delete>, was: <$COMMAND>"
    exit 1 
fi

echo "Deleted backup (backup_id='$ID')"
