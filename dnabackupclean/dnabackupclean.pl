#!/usr/bin/perl
# Delete CISCO DNA Center backups older than X days
# uses ciscodnacbackupctl to get the backup list and delete all IDs older then 
# the given number of days (default 30 days if no explicit value passed) 
#  
# usage: dnabackupclean [-a <max backup file age in days>] [-d]
# -a XXX max age in XXX days backups are kept, older ones are deleted
# -d dry-run, does only print the IDs that would be deleted
#
#
# Copyright (c)2021 Alexander Haas
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to deal 
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished 
# to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE


use strict;
use warnings;
use Time::Local;
use Getopt::Std;

#------------------------------------------------------------------------------#
# default settings
#------------------------------------------------------------------------------#
$Getopt::Std::STANDARD_HELP_VERSION = 1;
my $VERSION = "0.1";
my $maxage = 30;  # default max age value
my $dryrun = undef;



#------------------------------------------------------------------------------#
# DNA Center commands (ciscodnacbackupctl)
#------------------------------------------------------------------------------#

# mock commands - testing/debug only
my $listcommand = "./ciscodnacbackupctl_mock_list.sh list";
my $delcommand  = "./ciscodnacbackupctl_mock_delete.sh delete";

# real commands - change this to your installation
# Note: do not use ENV vars like $HOME here
#my $listcommand = "ciscodnacbackupctl list";
#my $delcommand = "ciscodnacbackupctl delete";



#------------------------------------------------------------------------------#
# process options
#------------------------------------------------------------------------------#

my %opts;
# -a=days of age 
# -d=dryrun
getopts('a:d', \%opts) or abort();

if (defined $opts{a}) {
  $maxage=$opts{a};
}
print "INFO: all backups older than $maxage days will be deleted\n";
if (defined $opts{d}) {
  $dryrun = 1;
  print "INFO: performing dryrun, no backups will be deleted - only IDs of too old backups will be printed\n";
}



#------------------------------------------------------------------------------#
# process backup list
#------------------------------------------------------------------------------#

my @listresult = `$listcommand`;
#print @listresult;

if (@listresult) {
    # remove first line that contains the column names
    shift (@listresult)
} else {
    printf "ABORTED: $listcommand did not return any output!";
    exit 2;
}


foreach (@listresult) {
    # syntax example:
    # 9902ebc8-e4d1-48d0-b8e4-ceab49641a82  107G           TRUE          daily-backup   2021-05-11 23:04:15  2021-05-11 23:01:02  SUCCESS
    my ($id,$size,$compat,$desc,$enddate,$endtime,$startdate,$starttime,$status) = /(\S{36})\s+(\S+)\s+(\w+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\w+)/;
    #debug print "$id, $size, $compat, $desc, $enddate, $endtime, $startdate, $starttime, $status\n"; 
    
    if (isOlderThanAge($enddate, $endtime, $maxage)) {
        if ($dryrun) {
            print "DRYRUN: $id wold be deleted - $enddate $endtime is older than $maxage days\n";
        } else {
            print "INFO: deleting $id\n";
            system "$delcommand $id";
        }
    }
}




#------------------------------------------------------------------------------#
# Subroutines 
#------------------------------------------------------------------------------#

sub isOlderThanAge {
    my($checkDate, $checkTime, $maxage) = @_;
    my $isValid = 1;
    my $isOlder = 0;
    
    my ($year,$month,$day) = $checkDate=~/^(\d{4})-(\d{2})-(\d{2})$/ or $isValid=0;
    my ($hour,$min,$sec) = $checkTime=~/^(\d{2}):(\d{2}):(\d{2})$/ or $isValid=0;
    
    
    if ($isValid) {
        #debug print "checkdate+time = $year, $month, $day, $hour, $min, $sec \n";
        $month = $month-1; # timelocal expects months between 0 (January) and 11 (December), but the checkDate-String delivers 1-12
        my $maxageInSecs = $maxage * 24 * 3600;  # a day has 24 hours a 3600 secs
        my $checkDateInEpochSecs = timelocal($sec, $min, $hour, $day, $month, $year);
        my $currentDateInEpocSecs = time();
    
        if ($currentDateInEpocSecs - $maxageInSecs > $checkDateInEpochSecs) {
            $isOlder=1;
        }
    } else {
        print "ERROR: $checkDate, $checkTime are not vaild date/time strings - skipping! \n"; 
    }
    return $isOlder;
}



#------------------------------------------------------------------------------#
# Subroutines for options processing  
# see https://gist.github.com/weibeld/72ae500a602fcf2253eceaa6e3d3fdce
# thanks to Daniel Weibel
#------------------------------------------------------------------------------#

# Called by Getopt::Std when supplying --help option
sub HELP_MESSAGE {
  print get_help_message();
}

# Called by Getopt::Std when supplying --version or --help option
sub VERSION_MESSAGE {
  print "Version $VERSION\n";
}

# Abort script due to error (e.g. invalid command line options)
sub abort {
  print get_help_message();
  exit 1;
}

# Central help message
sub get_help_message {
  return "Usage: dnabackupclean -a <max backup file age in days>)
Add -d for dry run - only prints the IDs of the older backup files instead of deleting them
If -a is omitted, $maxage days is used\n";
}
