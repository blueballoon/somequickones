#!/usr/bin/perl
# rescales a set of images to a give maximum value and writes them to a 
# given target directory (does not modify the originals, except 
# targetDir = sourceDir) 
# usage: rescaleImages.pl targetDirectory maxImageSize(Pixel) file(s) 
#
# Copyright (c)2013 Alexander Haas
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to deal 
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished 
# to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

use Image::Magick;
use File::Basename;
use strict;
use warnings;

my $targetDir = shift @ARGV;
my $maxDimSize = shift @ARGV;
my @origFiles = @ARGV;
my $retVal = 0;

print "Scaling Images to: $targetDir, $maxDimSize \n";
if (! -d $targetDir )
{
	mkdir $targetDir or  die "error: cannot create  $targetDir: $!";
}

foreach my $curFile (@origFiles)
{
	if (-f $curFile)
	{
        my $imgProcessor = new Image::Magick;
        $retVal = $imgProcessor->Read( $curFile);
        if ($retVal)
        {
			print "warning: cannot process  $curFile:\n";
			warn "$retVal \n";
		}
		else
		{
            my $width = $imgProcessor->Get('width');
            my $height = $imgProcessor->Get('height');
            #print "Image $curFile is $width x $height \n";
        
            my $maxDimOrig=$width;
            my $orientation = "landscape";
            if ($maxDimOrig<$height)
            {
	    		$maxDimOrig=$height;
	    		$orientation="portrait"; 
    		}
		
		    if ($maxDimOrig > $maxDimSize)
		    {
			    my $ratio = $width / $height;
			   # my $newGeom = "0x0";
			   
			    my $newWidth = 0;
			    my $newHeight= 0;
			    if ($orientation eq "landscape")
			    {
					$newWidth = $maxDimSize;
					$newHeight= $newWidth / $ratio;
				}
				else
				{
					$newHeight=$maxDimSize;
					$newWidth = $newHeight * $ratio;
				}
				
				my $fileBaseName = basename $curFile;
				my $newFileName = $targetDir . "/" . $fileBaseName;
			    print "info: scaling $curFile from $width x $height to $newFileName at $newWidth x $newHeight \n";
			    $retVal = $imgProcessor->Resize(width=>$newWidth,
			                                    height=>$newHeight,
			                                    filter=>"Cubic");
                if ($retVal)
                {       
			        print "warning: problem during scaling of  $fileBaseName:\n";
			        warn "$retVal \n";
		        }	
		        else
		        {
					$retVal = $imgProcessor->Write($newFileName);
					if ($retVal)
                    {       
			            print "warning: problem during writing of $newFileName:\n";
			            warn "$retVal \n";
		            }
				}
					        		                                    
		    }
		    else
		    {
				print "info: no processing necessary for $curFile ($width x $height) as it is already smaller than $maxDimSize\n ";
			}
	    }
        undef $imgProcessor;
    }
    else
    {
		print "warning: $curFile is not a regular file - ignoring \n";
	}
}
